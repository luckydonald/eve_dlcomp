
import argparse, fileinput, re, os.path, json, shutil

parser = argparse.ArgumentParser(description='Displaylist compiler/assembler and uploader for the FT800')

parser.add_argument('-p', '--port', action = "store", help = 'Serial port used for communication with the Display')
parser.add_argument('-o', '--output', action = "store", default = "DISP", help = 'The file to output the compiled Binary to. Use "DISP" to upload.')
parser.add_argument('--graphicsoutput', action = "store", help = 'The file to output the graphics Buffer to.')
parser.add_argument('--loadimages', action = "store_true", help = 'Convert and Upload images')
parser.add_argument('--imagedir', action = "store", default = "", help = 'Directory where the images are stored')
parser.add_argument('--forcereload', action = "store_true", help = 'Force the Compiler to reload all Images')
parser.add_argument('input', help = 'The file to Compile')

args = parser.parse_args()

doUpload = False

if args.output == "DISP":
    if args.port != None:
        doUpload = True
        import serial
    else:
        print("Error: No Serial-port and no Output-file given.")
        exit(-12)

if args.loadimages:
    from PIL import Image

fi = fileinput.input(args.input)

RAM_G_Size = 0x3FFFF
RAM_DL_Size = 0x1FFF

rawline = "Dummy"

paramRegex = r"(?P<G{0:02d}P{1}D>\d+)|(?P<G{0:02d}P{1}H>0[xX][a-fA-F0-9]+)|(?P<G{0:02d}P{1}S>[\s\w*/+-<>]+)"

constants = {
    "ALPHA_NEVER"                       :0,
    "ALPHA_LESS"                        :1,
    "ALPHA_LESS_EQUAL"                  :2,
    "ALPHA_GREATER"                     :3,
    "ALPHA_GREATER_EQUAL"               :4,
    "ALPHA_EQUAL"                       :5,
    "ALPHA_NOTEQUAL"                    :6,
    "ALPHA_ALWAYS"                      :7,

    "DRAW_TYPE_BITMAPS"                 :1,
    "DRAW_TYPE_POINTS"                  :2,
    "DRAW_TYPE_LINES"                   :3,
    "DRAW_TYPE_LINE_STRIP"              :4,
    "DRAW_TYPE_EDGE_STRIP_R"            :5,
    "DRAW_TYPE_EDGE_STRIP_L"            :6,
    "DRAW_TYPE_EDGE_STRIP_A"            :7,
    "DRAW_TYPE_EDGE_STRIP_B"            :8,
    "DRAW_TYPE_RECTS"                   :9,

    "BITMAP_FORMAT_ARGB1555"            :0,
    "BITMAP_FORMAT_L1"                  :1,
    "BITMAP_FORMAT_L4"                  :2,
    "BITMAP_FORMAT_L8"                  :3,
    "BITMAP_FORMAT_RGB332"              :4,
    "BITMAP_FORMAT_ARGB2"               :5,
    "BITMAP_FORMAT_ARGB4"               :6,
    "BITMAP_FORMAT_RGB565"              :7,
    "BITMAP_FORMAT_PALTTED"             :8,
    "BITMAP_FORMAT_TEXT8X8"             :9,
    "BITMAP_FORMAT_TEXTVGA"             :10,
    "BITMAP_FORMAT_BARGRAPH"            :11,

    "BLEND_FUNC_ZERO"                   :0,
    "BLEND_FUNC_ONE"                    :1,
    "BLEND_FUNC_SRC_ALPHA"              :2,
    "BLEND_FUNC_DST_ALPHA"              :3,
    "BLEND_FUNC_ONE_MINUS_SRC_ALPHA"    :4,
    "BLEND_FUNC_ONE_MINUS_DST_ALPHA"    :5,

    "STENCIL_OP_ZERO"                   :0,
    "STENCIL_OP_KEEP"                   :1,
    "STENCIL_OP_REPLACE"                :2,
    "STENCIL_OP_INCR"                   :3,
    "STENCIL_OP_DECR"                   :4,
    "STENCIL_OP_INVERT"                 :5
    }

constatPrefixes = [
    "ALPHA",
    "DRAW_TYPE",
    "BITMAP_FORMAT",
    "BITMAP_FORMAT",
    "BLEND_FUNC",
    "STENCIL_OP"
    ]

allCommands = [
    [
    ("ALPHA_FUNC"               , 0x09, [(0x07, 8), (0xFF, 0)]),
    ("STENCIL_FUNC"             , 0x0A, [(0x07, 16), (0xFF, 8), (0xFF, 0)]),
    
    ("STENCIL_MASK"             , 0x13, [(0xFF, 0)]),
    ("STENCIL_OP"               , 0x0C, [(0x07, 3), (0x07, 0)]),
    
    ("TAG"                      , 0x03, [(0xFF, 0)]),
    ("TAG_MASK"                 , 0x14, [(0x01, 0)]),
    
    ("BEGIN"                    , 0x1F, [(0x0F, 0)]),
    ("END"                      , 0x21, []),
    ("DISPLAY"                  , 0x00, []),
    
    ("VERTEX2F"                 , 0x40, [(0x7FFF, 15), (0x7FFF, 0)]),
    ("VERTEX2II"                , 0x80, [(0x1FF, 21), (0x1FF, 12), (0x0F, 7), (0x7F, 0)])
    ],[
    ("CLEAR"                    , 0x26, [(0x01, 2), (0x01, 1), (0x01, 0)]),
    ("CLEAR_COLOR_A"            , 0x0F, [(0xFF, 0)]),
    ("CLEAR_COLOR_RGB"          , 0x02, [(0xFF, 16), (0xFF, 8), (0xFF, 0)]),
    ("CLEAR_STENCIL"            , 0x11, [(0xFF, 0)]),
    ("CLEAR_TAG"                , 0x12, [(0xFF, 0)]),
    
    ("COLOR_A"                  , 0x10, [(0xFF, 0)]),
    ("COLOR_MASK"               , 0x20, [(0x01, 3), (0x01, 2), (0x01, 1), (0x01, 0)]),
    ("COLOR_RGB"                , 0x04, [(0xFF, 16), (0xFF, 8), (0xFF, 0)]),

    ("LINE_WIDTH"               , 0x0E, [(0xFFF, 0)]),
    ("POINT_SIZE"               , 0x0D, [(0x1FFF, 0)])
    ],[
    ("SAVE_CONTEXT"             , 0x22, []),
    ("RESTORE_CONTEXT"          , 0x23, []),

    ("SCISSOR_XY"               , 0x1B, [(0x1FF, 9), (0x1FF, 0)]),
    ("SCISSOR_SIZE"             , 0x1C, [(0x3FF, 10), (0x3FF, 0)]),

    ("CELL"                     , 0x06, [(0x7F, 0)]),
    ("BLEND_FUNC"               , 0x11, [(0x07, 3), (0x07, 0)]),

    ("CALL"                     , 0x1D, [(0xFFFF, 0)]),
    ("JUMP"                     , 0x1E, [(0xFFFF, 0)]),
    ("RETURN"                   , 0x24, []),
    ("MACRO"                    , 0x25, [(0x01, 0)])
    ],[
    ("BITMAP_HANDLE"            , 0x05, [(0x1F, 0)]),
    ("BITMAP_LAYOUT"            , 0x07, [(0x1F, 19), (0x3FF, 9), (0x1FF, 0)]),
    ("BITMAP_SIZE"              , 0x08, [(0x01, 20), (0x01, 19), (0x01, 18), (0x1FF, 9), (0x1FF, 0)]),
    ("BITMAP_SOURCE"            , 0x01, [(0x1FFFF, 0)]),
    ("BITMAP_TRANSFORM_A"       , 0x15, [(0x1FFFF, 0)]),
    ("BITMAP_TRANSFORM_B"       , 0x16, [(0x1FFFF, 0)]),
    ("BITMAP_TRANSFORM_C"       , 0x17, [(0xFFFFFF, 0)]),
    ("BITMAP_TRANSFORM_D"       , 0x18, [(0x1FFFF, 0)]),
    ("BITMAP_TRANSFORM_E"       , 0x19, [(0x1FFFF, 0)]),
    ("BITMAP_TRANSFORM_F"       , 0x1A, [(0xFFFFFF, 0)])
    ]
]

#       ("" , 0x02, [(0xFF, 16), (0xFF, 8), (0xFF, 0)]),

allMatchers = []
allLookups = []

for commands in allCommands:
    regex = ""             
    lookup = {}
    index = 0
    for command in commands:
        cmdName, opCode, params = command
        paramsRegex = ""
        paramIndex = 0
        lookupParams = []
        for param in params:
            lookupParams += [param]
            reg = paramRegex.format(index, paramIndex)
            paramsRegex += ("," if paramsRegex else "") + (r"\s*(%s)\s*" % reg)
            paramIndex += 1

        lookup[index] = (opCode, lookupParams)
        regexPart = r"(?P<G%03i>(:EVE_)?%s\s*\(%s\)\s*;?)" % (index, cmdName, paramsRegex)
        regex += ("|" if index else "") + regexPart
        index += 1
    matcher = re.compile(regex).search
    allMatchers += [matcher]
    allLookups += [lookup]

graphicsBuffer = []

if not os.path.exists(".evecache"):
    os.mkdir(".evecache")

variables = {}
if os.path.exists(".evecache/vars.json"):
    f = open(".evecache/vars.json", "r")
    variables = json.load(f)
    f.close()

def loadImage(filename, imgformat, alias):
    global graphicsBuffer
    offset = len(graphicsBuffer)
    formatName = imgformat.split("_")[-1].lower()
    if (not args.forcereload) and os.path.exists(".evecache/%s.%s.bin" % (filename, formatName)):
        f = open(".evecache/%s.%s.bin" % (filename, formatName), "rb")
        graphicsBuffer += list(f.read())
        f.close()
    else:
        img = Image.open(os.path.join(args.imagedir, filename))
        width, heigth = img.size
        data = {}
        formatName = ""
        for band in img.getbands():
            data[band] = list(img.getdata(len(data)))
        if imgformat.endswith("L1"):
            for y in range(heigth):
                for x in range(0, width, 8):
                    bs = 0
                    for i in range(8):
                        r, g, b = data["R"][y*width + x + i], data["G"][y*width + x + i], data["B"][y*width + x + i]
                        yv = 0.299 * r + 0.587 * g + 0.114 * b
                        bs |= (int(min(0xFF, max(0, yv))) & 0x80) >> i
                    graphicsBuffer += [bs]
            formatName = "l1"
            linestride = int(width / 8)
        elif imgformat.endswith("L4"):
            for y in range(heigth):
                for x in range(0, width, 2):
                    bs = 0
                    for i in range(2):
                        r, g, b = data["R"][y*width + x + i], data["G"][y*width + x + i], data["B"][y*width + x + i]
                        yv = 0.299 * r + 0.587 * g + 0.114 * b
                        bs |= ((int(min(0xFF, max(0, yv))) & 0xF0) >> (i * 4)) & 0xFF
                    graphicsBuffer += [bs]
            formatName = "l4"
            linestride = int(width / 2)
        elif imgformat.endswith("L8"):
            for y in range(heigth):
                for x in range(0, width):
                    r, g, b = data["R"][y*width + x], data["G"][y*width + x], data["B"][y*width + x]
                    yv = 0.299 * r + 0.587 * g + 0.114 * b
                    bs = int(min(0xFF, max(0, yv))) & 0xFF
                    graphicsBuffer += [bs]
            formatName = "l8"
            linestride = width
        elif imgformat.endswith("ARGB4"):
            for i in range(width * heigth):
                r, g, b = data["R"][i], data["G"][i], data["B"][i]
                a = data["A"][i] if "A" in data else 255
                graphicsBuffer += [(((b & 0xF0) >> 4) | (g & 0xF0)) & 0xFF]
                graphicsBuffer += [(((r & 0xF0) >> 4) | (a & 0xF0)) & 0xFF]
            formatName = "argb4"
            linestride = width * 2
        elif imgformat.endswith("ARGB2"):
            for i in range(width * heigth):
                r, g, b = data["R"][i], data["G"][i], data["B"][i]
                a = data["A"][i] if "A" in data else 255
                graphicsBuffer += [(((b & 0xA0) >> 6) | ((g & 0xA0) >> 4) | ((r & 0xA0) >> 2) | (a & 0xA0)) & 0xFF]
            formatName = "argb2"
            linestride = width
        else:
            print("Error: Unknown format '%s'" % imgformat)
            return False
        variables[alias + ".width"] = width
        variables[alias + ".linestride"] = linestride
        variables[alias + ".heigth"] = heigth
        f = open(".evecache/%s.%s.bin" % (filename, formatName), "wb")
        f.write(bytes(graphicsBuffer[offset:]))
        f.close()
    variables[alias + ".data"] = offset
    return True


commandBuffer = []
lineNum = 0

while len(rawline) > 0:
    rawline = fi.readline()
    lineNum += 1
    line = rawline.strip()
    if line.startswith("//"):
        continue
    if line.startswith("#"):
        if line.startswith("#loadimage"):
            if args.loadimages:
                match = re.search('\(\s*"([\w\.-]+)"\s*,\s*(\w+)\s*,\s*(\w+)\s*\)', line)
                if not loadImage(*match.groups()):
                    print("Error on line %i: See Above '%s'" % (lineNum, line))
            else:
                print("Info: Skiping loadimage()")
        else:
            print("Warning on line %i: Unknown Metacommand '%s'" % (lineNum, line))
        continue
    currentMatcher = -1
    match = None
    while match == None and currentMatcher < len(allMatchers) - 1:
        currentMatcher += 1
        match = allMatchers[currentMatcher](line)
               
    if match:
        matches = match.groupdict()
        cmdId = int(match.lastgroup[1:])
        opCode, params = allLookups[currentMatcher][cmdId]
        opCode <<= 24
        paramIndex = 0
        for param in params:
            mask, shift = param
            groupName = "G%02iP%i" % (cmdId, paramIndex)
            if matches[groupName + "D"]:
                opCode |= ((int(matches[groupName + "D"]) & mask) << shift)
            elif matches[groupName + "H"]:
                opCode |= ((int(matches[groupName + "H"][2:], 16) & mask) << shift)
            elif matches[groupName + "S"]:
                string = matches[groupName + "S"]
                val = -1
                if string in constants:
                    val = constants[string]
                elif string in variables:
                    val = variables[string]
                else:
                    for prefix in constatPrefixes:
                        if prefix + "_" + string in constants:
                            val = constants[prefix + "_" + string]
                if val == -1:
                    val = eval(string)
                opCode |= ((val & mask) << shift)
            paramIndex += 1
        #print("0x%08X" % opCode)
        
        commandBuffer += bytearray([opCode& 0xFF, (opCode >> 8) & 0xFF, (opCode >> 16) & 0xFF, (opCode >> 24) & 0xFF])
        #commandBuffer += bytearray([(opCode >> 24) & 0xFF, (opCode >> 16) & 0xFF, (opCode >> 8) & 0xFF, opCode& 0xFF])
    elif line != "":
        print("Error on line %3i: could not resolve '%s'" % (lineNum, line))

#ser.write([0x80, 0x24, 0x10, 9, 0xFF, 0x00, 0x00, 0x00, 0x45, 0x3C, 0x00, 0x00, 0x01])


print("Displaylist: Using %s/%s Byte (%i%%)" % (len(commandBuffer), RAM_DL_Size, ((len(commandBuffer) / RAM_DL_Size) * 100)))
if len(commandBuffer) > RAM_DL_Size:
    print("Error: out of Memory")
    exit(-23)
    
if args.loadimages:
    print("Graphics Ram: Using %s/%s Byte (%i%%)" % (len(graphicsBuffer), RAM_G_Size, ((len(graphicsBuffer) / RAM_G_Size) * 100)))
    if len(graphicsBuffer) > RAM_G_Size:
        print("Error: out of Memory")
        exit(-23)


if doUpload:
    
    ser = serial.Serial(args.port, 5000000, timeout=1)
    
    sent = 0
    
    if args.loadimages:
        print("Uploading Images!")
        offset = 0
        while offset < len(graphicsBuffer):
            sent += ser.write([offset & 0xFF, (offset >> 8) & 0xFF, (offset >> 16) & 0x03, 60] + graphicsBuffer[offset:offset + 60])
            offset += 60

    print("Uploading Displaylist!")

    offset = 0
    
    while offset < len(commandBuffer):
        sent += ser.write([offset & 0xFF, (offset >> 8) & 0xFF, 0x10, 60] + commandBuffer[offset:offset + 60])
        offset += 60

    print("Upload complete! (total Bytes sent: %i)" % sent)

    ser.write([0x50, 0x24, 0x10, 1, 0x02])
    
    ser.close()
else:
    print("Writing Displaylist binary to %s" % args.output)
    f = open(args.output, "wb")
    b = bytes(commandBuffer);
    
    f.write(b)
    f.close()

if args.graphicsoutput:
    print("Writing Graphics Buffer to %s" % args.graphicsoutput)
    f = open(args.graphicsoutput, "wb")
    b = bytes(graphicsBuffer);
    
    f.write(b)
    f.close()

f = open(".evecache/vars.json", "w")
json.dump(variables, f)
f.close()

print("Done! Bye Bye. ")
