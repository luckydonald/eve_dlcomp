# EVE_DLComp #

EVE_DLComp is a compiler/assembler for FT800 Displaylists.

Support for the CO-Processor is planed but not yet implemented.

### Dependencies ###

* Python 3.x (Tested against v3.3.2:d047928ae3f6 win32)
* Pillow 2.9.0
* pyserial 2.7

### Usage ###


```
#!none

usage: EVE-DLComp.py [-h] [-p PORT] [-o OUTPUT]
                     [--graphicsoutput GRAPHICSOUTPUT] [--loadimages]
                     [--imagedir IMAGEDIR] [--forcereload]
                     input

Displaylist compiler/assembler and uploader for the FT800

positional arguments:
  input                 The file to Compile

optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT  Serial port used for communication with the Display
  -o OUTPUT, --output OUTPUT
                        The file to output the compiled Binary to. Use "DISP"
                        to upload.
  --graphicsoutput GRAPHICSOUTPUT
                        The file to output the graphics Buffer to.
  --loadimages          Convert and Upload images
  --imagedir IMAGEDIR   Directory where the images are stored
  --forcereload         Force the Compiler to reload all Images

```

#### Examples ####

Assuming the Display on COM19.


Compile and Upload test.edl and the images.

```
#!batch

python EVE_DLComp.py --loadimages -p "COM19" "test.edl"
```

Compile "test.edl" and save the binary to "test.cdl".

```
#!batch

python EVE_DLComp.py -o "test.cdl" "test.edl"
```

Compile "test.edl" and save the binary to "test.cdl".  
Load all specified images from the directory "img" and save the converted data to "g-ram.bin".  
Force the converter to reload all images even if they are cached.

```
#!batch

python EVE_DLComp.py --graphicsoutput "g-ram.bin" --loadimages --imagedir "img/" --forcereload -o "test.cdl" "test.edl"
```